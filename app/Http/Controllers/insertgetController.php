<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class insertgetController extends Controller
{
    
    
    public function insert(Request $req)
        
    {
         $firstname = $req->input('firstname');
         $lastname = $req->input('lastname');
         
        $data = array('firstname'=>$firstname,'lastname'=>$lastname);
        
        DB::table('laravels')->insert($data);
        
        
       return redirect('insert');
        
        
    }
    
    public function get()
        
    {
        
        $data['data'] = DB::table('laravels')->get();
        
        if(count($data) > 0)
        {
            
            return view('insertData',$data);
            
            // return redirect('insertData');
            
        }
        else{
            
            
            return view('insertData');
        }
        
        
        
    }
    
    
    public function delete()
    
    {
        
        
        DB::table('laravels')->delete();
        
        return redirect('insert');
        
    }
    
    
    
 
    
}
