
<html>
<head>
<style>


td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>

<table>
  <tr>
      <th>Id</th>
    <th>Name</th>
    
    
  </tr>
    
   @foreach($pages as $page)
  <tr>
      <td>{{$loop->index+1}}</td>
    <td>{{$page->title}}</td>
  
  </tr>
   @endforeach
    
    
</table>
{{$pages->links()}}
</body>
</html>
