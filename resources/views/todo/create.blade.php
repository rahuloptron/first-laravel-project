@extends('layouts.apps')

@section('body')

<br>

<a href="/todo" class="btn btn-info">back</a>

<div class="col-lg-4 col-lg-offset-4">

    <h1>{{substr(Route::currentRouteName(),5)}} item</h1>
    
    <form class="form-horizontal" action="/todo/@yield('editId')" method="post">
        
        {{csrf_field()}}
       @section('editMethod')
        @show
  <fieldset>
    
    <div class="form-group">
        
        <div class="col-lg-10">
        <input type="text" name="title" class="form-control" value="@yield('editTitle')" placeholder="Title">
      </div>
      
      <div class="col-lg-10">
        <textarea class="form-control" rows="3" name="body" placeholder="Body">@yield('editBody')</textarea>
          <br>
          <button type="submit" class="btn btn-success">Submit</button>
    
      </div>
    </div>
    
      @if (count($errors)>0)
      <div class="alert alert-danger">
   @foreach($errors->all() as $error)

            {{$error}}
      
      @endforeach
      </div>
      @endif
  </fieldset>
</form>
    
    

</div>


@endsection