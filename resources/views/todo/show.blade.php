@extends('layouts.apps')


@section('body')

<div class="col-lg-offset-4 col-lg-4" >
<h1>{{$item->title}}</h1>
<h2>{{$item->body}}</h2>
</div>
@endsection