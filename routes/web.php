<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('todo','todoController');


Route::get('/',function(){
    
    return view('welcome');
    
});

Route::get('/insert', function () {
    return view('insertData');
});


Route::post('/insert','insertgetController@insert');

Route::get('/insert','insertgetController@get');



Route::get('/relation',"relationController@relation");


Route::get('/pagination',"pagination@users");

//Route::get('/test', function () {
    
  //  return view('test');
    
    
//});


//Route::get("index","test@index"); 


//Route::post("store","test@store");


//Route::get('/abc',function(){
    
    
  // return view('welcome');
        
    
//});

//Route::get('/abc','abc@abc');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
